import { NestFactory } from '@nestjs/core';
import { Transport, RedisOptions } from '@nestjs/microservices';
import { TokenModule } from './token.module';
import { ConfigService } from './services/config/config.service';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(TokenModule, {
    transport: Transport.REDIS,
    options: {
      url: (new ConfigService()).get('broker'),
    },
  } as RedisOptions);
  await app.listenAsync();
}
bootstrap();
